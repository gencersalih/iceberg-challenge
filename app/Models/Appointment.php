<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'contact_id',
        'planned_date',
        'planned_address',
        'estimated_departure_time',
        'back_time_after_appointment',
        'estimated_distance',
        'estimated_duration',
    ];
}
