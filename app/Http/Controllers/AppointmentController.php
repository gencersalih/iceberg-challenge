<?php

namespace App\Http\Controllers;

use App\Repositories\AppointmentRepository;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{
    private $appointment_repository;

    public function __construct(AppointmentRepository $appointment_repository){
        $this->middleware('auth');
        $this->appointment_repository = $appointment_repository;
    }

    public function save(Request $request) {
        return $this->appointment_repository->save($request);
    }

    public function find(Request $request){
        return $this->appointment_repository->find($request);
    }

    public function destroy($id){
        return $this->appointment_repository->destroy($id);
    }

    public function update(Request $request, $id){
        return $this->appointment_repository->update($request, $id);
    }
}
