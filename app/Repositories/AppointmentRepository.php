<?php

namespace App\Repositories;

use App\Models\Appointment;
use App\Models\Contact;
use Illuminate\Http\Request;

class AppointmentRepository {

    private $main_postal_code = 'cm27pj';

    public function save(Request $request) {


        $validator = $request->validate([
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'planned_address' => 'required',
            'planned_date' => 'required',
        ]);

        $user_id = auth()->id();

        $appointment_data = $this->getAppointmentData($validator, $user_id);

        return Appointment::create($appointment_data);
    }

    public function find(Request $request){

        $appointment = Appointment::query();

        $user_id = auth()->id();

        $appointment->where('user_id', '=', $user_id);

        if($request->has('order')){
            $order = $request->get('order');
            $appointment->orderBy('created_at', $order);
        }

        return $appointment->get();
    }

    public function destroy($id){
        $user_id = auth()->id();

        $appointment = Appointment::where(['id'=>$id, 'user_id' => $user_id])->first();
        if($appointment->delete()){
            return [
                'message' => 'Record deleted',
            ];
        }
        return [
            'message' => 'Record not deleted',
        ];

    }

    public function update(Request $request, $id){

        $validator = $request->validate([
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'planned_address' => 'required',
            'planned_date' => 'required',
        ]);

        $user_id = auth()->id();

        $appointment = Appointment::where(['id'=>$id, 'user_id' => $user_id])->first();

        $appointment_data = $this->getAppointmentData($validator, $user_id);

        $appointment->update($appointment_data);

        return $appointment;
    }

    private function getAppointmentData(array $validator, $user_id)
    {
        $contact_exists = Contact::where(['email' => $validator['email']])->first();

        if ($contact_exists) {
            $contact_id = $contact_exists->id;
        } else {

            $contact = new Contact();
            $contact->name = $validator['name'];
            $contact->surname = $validator['surname'];
            $contact->email = $validator['email'];
            $contact->address = $validator['address'];
            $contact->phone = $validator['phone'];

            $contact->save();
            $contact_id = $contact->id;

        }

        $distanceCalcResult = $this->getDistance($this->main_postal_code, $validator['planned_address']);

        $distance_meter = $distanceCalcResult['status'] == 'OK' ? intval($distanceCalcResult['rows'][0]['elements'][0]['distance']['value']) : 0;
        $distance_duration = $distanceCalcResult['status'] == 'OK' ? intval($distanceCalcResult['rows'][0]['elements'][0]['duration']['value']) : 0;

        $one_hour = (60 * 60);
        $planned_time = strtotime($validator['planned_date']);
        $planned_date = date("d-m-Y H:i:s",$planned_time);
        $estimated_date = date("d-m-Y H:i:s", $planned_time - $distance_duration);
        $estimated_date2 = date("d-m-Y H:i:s", $planned_time + $one_hour + $distance_duration);

        $appointment_data = [
            'user_id' => $user_id,
            'contact_id' => $contact_id,
            'planned_date' => $planned_date,
            'planned_address' => $validator['planned_address'],
            'estimated_distance' => $distance_meter,
            'estimated_duration' => $distance_duration,
            'estimated_departure_time' => $estimated_date,
            'back_time_after_appointment' => $estimated_date2,
        ];
        return $appointment_data;
    }

    private function getLookupPostalCode($postalCode){
        $url = "https://api.postcodes.io/postcodes/".$postalCode;
        $fg = file_get_contents($url);
        $locationResult = json_decode($fg, true);

        return $locationResult;
    }

    private function getDistance($startDestinationPoint, $endDestinationPoint){
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$startDestinationPoint."&destinations=".$endDestinationPoint."&key=AIzaSyCLRLXIzRPdqkcUR33tj4evocxmnWeTbRw";

        $fg = file_get_contents($url);
        return json_decode($fg, true);
    }
}
