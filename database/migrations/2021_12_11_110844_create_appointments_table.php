<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('contact_id');
            $table->string('planned_address');
            $table->dateTime('planned_date');
            $table->dateTime('estimated_departure_time')->nullable();
            $table->dateTime('back_time_after_appointment')->nullable();
            $table->integer('estimated_distance')->nullable();
            $table->integer('estimated_duration')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
